/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.command

import akka.actor.{Actor, ActorRef}
import de.garantiertnicht.supplyzepelin.bot.GuildCommand
import net.dv8tion.jda.api.Permission

import scala.collection.mutable

case class CommandExecution(guildCommand: GuildCommand, actor: ActorRef)

class CommandManager(commands: Command*) extends Actor {
  implicit val dispatcher = context.system.dispatcher

  val map = new mutable.HashMap[String, Command]
  commands.foreach(command => map += ((command.name.toLowerCase, command)))

  override def receive: Receive = {
    case CommandExecution(guildCommand, guild) =>
      val commandAndArgs = guildCommand.command.split(' ')
      if (commandAndArgs.nonEmpty && guildCommand.message.getGuild.getSelfMember
          .hasPermission(
            guildCommand.message.getTextChannel,
            Permission.MESSAGE_READ,
            Permission.MESSAGE_WRITE,
            Permission.MESSAGE_HISTORY,
            Permission.MESSAGE_EMBED_LINKS
          )) {
        val commandName = commandAndArgs(0).toLowerCase
        val command = map.get(commandName)

        command.foreach(command => {
          if (command.canary.isEmpty || command.canary.get
              .isEligible(guildCommand.message.getGuild)) {
            if (command.permission.isEmpty || guildCommand.message.getMember
                .hasPermission(command.permission.get)) {
              command.execute(commandAndArgs, guildCommand.message, guild)
            } else {
              guildCommand.message.getChannel
                .sendMessage(
                  "Sorry, but you are missing a permission: " + command.permission.get.getName
                )
                .queue()
            }
          }
        })
      }
  }
}
