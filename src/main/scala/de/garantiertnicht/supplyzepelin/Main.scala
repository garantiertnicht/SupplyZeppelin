/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin

import java.io.FileInputStream
import java.util.Properties
import akka.actor.{ActorRef, ActorSystem, CoordinatedShutdown, Props}
import akka.Done
import de.garantiertnicht.supplyzepelin.bot._
import de.garantiertnicht.supplyzepelin.persistence.Connection
import de.garantiertnicht.supplyzepelin.persistence.entities.Points
import de.garantiertnicht.supplyzepelin.persistence.entities.rewards.MemberUpdate
import net.dv8tion.jda.api.entities.{Activity, Emote, Member, TextChannel}
import net.dv8tion.jda.api.events.ReadyEvent
import net.dv8tion.jda.api.events.guild.GuildJoinEvent
import net.dv8tion.jda.api.events.guild.member.{GuildMemberJoinEvent, GuildMemberRoleAddEvent, GuildMemberRoleRemoveEvent}
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.api.events.message.guild.react.{GuildMessageReactionAddEvent, GuildMessageReactionRemoveEvent}
import net.dv8tion.jda.api.hooks.ListenerAdapter
import net.dv8tion.jda.api.requests.GatewayIntent
import net.dv8tion.jda.api.{AccountType, JDA, JDABuilder}
import net.dv8tion.jda.api.requests.GatewayIntent._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object Main {
  val system = ActorSystem("supply-zeppelin")
  var bot: ActorRef = _
  var reaction: Emote = _
  var emojiId: Long = _
  var infoId: Long = _
  var databasePassword: String = _
  var jda: JDA = _
  var superUser = -1L
  var databaseCollection: String = _
  var guild: Long = _
  var haxcidentChannelId: Long = _
  var haxcidentChannel: TextChannel = _
  val MAX_POINTS: Points = 1L << 62

  def maxPointsExceededMessage(member: Member) =
    s"Sorry, but you can not have more or equal then $MAX_POINTS, ${Bot.formatMember(member)}"

  val MAX_POINT_MODIFICATION: Points = 1L << 48

  def maxPointsModificationExceededMessage(member: Member) =
    s"Sorry, but you may not modify your points by an amount more or equal then $MAX_POINT_MODIFICATION points, ${Bot
      .formatMember(member)}"

  var tosEmote: Emote = _
  var tosEmoteId: String = _

  def tosClickEmote =
    s"""To view the Terms of Service and Privacy Policy for bots from
       |garantiertnicht software, please click ${tosEmote.getAsMention}.
       |""".stripMargin.lines.mkString(" ")

  var tosCode: String = _
  var supportCode: String = _

  def tosNotice =
    s"""To view the Terms of Service and Privacy Policy for bots from
      |garantiertnicht software: <https://discord.gg\\$tosCode>.
      |""".stripMargin.lines.mkString(" ")

  def main(args: Array[String]): Unit = {
    val properties = new Properties()
    val input = new FileInputStream("settings.properties")
    properties.load(input)

    val token = properties.getProperty("token")
    superUser = properties.getProperty("superUser").toLong
    emojiId = properties.getProperty("crateEmote").toLong
    databasePassword = properties.getProperty("databasePassword")
    databaseCollection = properties.getProperty("databaseCollection")
    tosEmoteId = properties.getProperty("tosEmote")
    guild = properties.getProperty("guild").toLong
    haxcidentChannelId = properties.getProperty("haxcidentChannel").toLong
    tosCode = properties.getProperty("tosInviteCode")
    supportCode = properties.getProperty("supportInviteCode")
    infoId = properties.getProperty("infoEmote").toLong

    input.close()

    Connection.waitUntilReady()

    jda = JDABuilder
      .create(
        token,
        GUILD_MESSAGES,
        GUILD_MESSAGE_REACTIONS,
        GUILD_MEMBERS,
      )
      .addEventListeners(Events)
      .setEnableShutdownHook(false)
      .setActivity(Activity.watching("@mention help"))
      .build()

    bot = system.actorOf(Props[Bot], "bot")

    CoordinatedShutdown(system).addTask(
      CoordinatedShutdown.PhaseServiceStop,
      "Shutting down JDA"
    ) {() =>
      Future {
        jda.shutdown()
        Done
      }
    }

  }
}

object Events extends ListenerAdapter {
  override def onReady(event: ReadyEvent): Unit = {
    val guild = event.getJDA.getGuildById(Main.guild)
    Main.reaction = guild.retrieveEmoteById(Main.emojiId).complete()
    Main.tosEmote = guild.retrieveEmoteById(Main.tosEmoteId).complete()
    Main.haxcidentChannel = guild.getTextChannelById(Main.haxcidentChannelId)
  }

  override def onGuildMessageReceived(event: GuildMessageReceivedEvent): Unit =
    Main.bot ! GuildMessage(event.getMessage)

  override def onGuildMessageReactionAdd(
    event: GuildMessageReactionAddEvent
  ): Unit =
    Main.bot ! GuildReaction(event.getReaction, event.getMember)

  override def onGuildMessageReactionRemove(
    event: GuildMessageReactionRemoveEvent
  ): Unit =
    Main.bot ! GuildReactionRemoved(event.getReaction, event.getMember)

  override def onGuildMemberJoin(event: GuildMemberJoinEvent) =
    Main.bot ! Recalculate(event.getGuild, MemberUpdate(), Seq(event.getMember))

  override def onGuildJoin(event: GuildJoinEvent) =
    Main.bot ! Recalculate(event.getGuild, MemberUpdate())

  override def onGuildMemberRoleAdd(event: GuildMemberRoleAddEvent) =
    Main.bot ! Recalculate(event.getGuild, MemberUpdate(), Seq(event.getMember))

  override def onGuildMemberRoleRemove(event: GuildMemberRoleRemoveEvent) =
    Main.bot ! Recalculate(event.getGuild, MemberUpdate(), Seq(event.getMember))
}
