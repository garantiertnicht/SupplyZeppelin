/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot

import java.util.concurrent.TimeUnit

import akka.actor.Actor
import com.mongodb.client.model.UpdateOptions
import de.garantiertnicht.supplyzepelin.bot.deployment.Rewards
import de.garantiertnicht.supplyzepelin.bot.utils.{RankedMember, Ranker}
import de.garantiertnicht.supplyzepelin.persistence.Connection
import de.garantiertnicht.supplyzepelin.persistence.definitions.{
  GetRewards,
  GetScoreboard
}
import de.garantiertnicht.supplyzepelin.persistence.entities.rewards.{
  Event,
  Reward,
  RewardData
}
import de.garantiertnicht.supplyzepelin.persistence.entities.{
  RewardInfo,
  ScoreboardEntry
}
import net.dv8tion.jda.api.entities.{Guild, Member}
import org.mongodb.scala.model.Filters.{equal, or}
import org.mongodb.scala.model.Updates.{pull, set}

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}
import scala.util.{Failure, Success}

case class Recalculate(
  guild: Guild,
  source: Event,
  members: Seq[Member] = Seq()
)

case class RemoveRewardFromMembers(guild: Guild, oldReward: Reward)

class RewardActor extends Actor {
  implicit val executionContext: ExecutionContext = context.system.dispatcher

  override def receive: PartialFunction[Any, Unit] = {
    case Recalculate(guild, source, specifiedMembers) ⇒
      val getRewards = new GetRewards(guild.getIdLong)
      getRewards.mongoQuery.toFuture().onComplete {
        case Success(rewards) ⇒
          val members = if (specifiedMembers.isEmpty) {
            guild.getMembers.asScala
          } else {
            specifiedMembers
          }

          calculate(guild, members, getRewards.withManaged(rewards), source)
        case Failure(e) =>
          e.printStackTrace()
      }
    case RemoveRewardFromMembers(guild, oldReward) ⇒
      remove(guild, guild.getMembers.asScala, oldReward)
  }

  def remove(guild: Guild, members: Seq[Member], oldReward: Reward): Unit = {
    val ranked = if (oldReward.options.remove) {
      applyToAll(guild, members, oldReward.reverse)
    } else {
      RewardActor
        .mapMembersToRankedMembers(members, guild)
        .filter(_.entry.rewards.granted.contains(oldReward._id.name))
    }

    Connection.scoreboard
      .updateMany(
        or(ranked.map(member ⇒ equal("_id", member.entry._id)): _*),
        pull("rewards.granted", oldReward._id.name)
      )
      .head()
  }

  def applyToAll(
    guild: Guild,
    members: Seq[Member],
    reward: Reward
  ): Seq[RankedMember] = {
    val appliedReward = reward.squash.getOrElse(
      Reward(reward._id, RewardData(Seq(), Seq()), None)
    )
    val ranked = RewardActor.mapMembersToRankedMembers(members, guild)

    if (!reward.options.enabled) return ranked

    ranked
      .filter(
        _.entry.rewards.exists(_.granted.contains(appliedReward._id.name))
      )
      .foreach { member ⇒
        appliedReward.applyReward(member.member.get)
      }

    ranked
  }

  def calculate(
    guild: Guild,
    members: Seq[Member],
    rewards: Seq[Reward],
    source: Event,
    invocation: Int = 0
  ): Unit = {
    if (!Rewards.isEligible(guild)) {
      return
    }

    val orderedRewards =
      rewards.filter(_.options.enabled).sortBy(_.options.priority)

    if (members.isEmpty || rewards.isEmpty) {
      return
    }

    if (invocation >= 3) {
      println(s"Runaway rewards for ${guild.getName} (${guild.getId})")
      return
    }

    val emptyRewardData = RewardData(Seq(), Seq())
    val upsert = new UpdateOptions().upsert(true)

    val memberIds = members.map(_.getUser.getIdLong)
    RewardActor
      .mapMembersToRankedMembers(members, guild)
      .filter(ranked ⇒ memberIds.contains(ranked.entry._id.userId))
      .foreach(rankedMember ⇒ {
        val oldRewards = orderedRewards.filter(
          reward => rankedMember.entry.rewards.granted.contains(reward._id.name)
        )
        val newRewards: Seq[Reward] = (orderedRewards.filter(
          reward ⇒
            !oldRewards.contains(reward) && reward
              .requirementsMet(rankedMember, source, false)
        ) ++: oldRewards
          .filter(
            reward ⇒
              !reward.options.remove || reward
                .requirementsMet(rankedMember, source, true)
          )).distinct.sortBy(_.options.priority)
        val persistedRewards = newRewards.filter(_.options.persist)

        val newCache = if (persistedRewards.isEmpty) {
          Seq()
        } else {
          persistedRewards
            .map(_.data)
            .reduceRight((a, b) ⇒ a.merge(b).squash.getOrElse(emptyRewardData))
            .grants
            .filter(_.cached)
        }

        val rewardsRemoved = oldRewards.filterNot(newRewards.contains)
        val reallyNewAwards = newRewards.filterNot(oldRewards.contains)
        val dirtyRewards = persistedRewards
          .filterNot(reallyNewAwards.contains)
          .map(_.onlyDirty)
        val rewardsAdded: Seq[Reward] = dirtyRewards ++: reallyNewAwards

        if (rewardsAdded.nonEmpty || rewardsRemoved.nonEmpty) {
          rankedMember.member.foreach(member ⇒ {
            val reward =
              (rewardsAdded ++: rewardsRemoved.map(_.reverse))
                .sortBy(_.options.priority)
                .map(_.data)
                .reduceLeft(
                  (a, b) ⇒ a.merge(b).squash.getOrElse(emptyRewardData)
                )

            reward.applyReward(member)

            if (!rankedMember.entry.rewards
                .exists(_.granted == persistedRewards.map(_._id.name))) {
              Connection.scoreboard
                .updateOne(
                  equal("_id", rankedMember.entry._id),
                  set(
                    "rewards",
                    RewardInfo(
                      persistedRewards
                        .map(_._id.name),
                      Some(newCache)
                    )
                  ),
                  upsert
                )
                .head()
            }
          })
        }
      })
  }
}

object RewardActor {
  def mapMembersToRankedMembers(
    members: Seq[Member],
    guild: Guild
  ): Seq[RankedMember] = {
    val eligibleMembers = guild.getMembers.asScala.filterNot(_.getUser.isBot)

    val informationAuthoritative = Await
      .result(
        new GetScoreboard(guild).mongoQuery.toFuture(),
        Duration(1, TimeUnit.MINUTES)
      )
      .filter(
        info =>
          eligibleMembers.map(_.getUser.getIdLong).contains(info._id.userId)
      )
    val information: Seq[ScoreboardEntry] = eligibleMembers
      .filter(
        member ⇒
          !informationAuthoritative
            .map(_._id.userId)
            .contains(member.getUser.getIdLong)
      )
      .map(ScoreboardEntry.default(_)) ++: informationAuthoritative

    Ranker
      .rank(information)
      .filter(
        ranked ⇒
          ranked.member.isDefined &&
            ranked.member.get.getGuild.getIdLong == guild.getIdLong &&
            !ranked.entry.blocked.contains(true) &&
            members.contains(ranked.member.get)
      )
  }
}
