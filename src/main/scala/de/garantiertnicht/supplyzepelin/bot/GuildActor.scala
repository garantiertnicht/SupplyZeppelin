/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot

import akka.actor.{Actor, PoisonPill, ReceiveTimeout}
import akka.event.Logging
import akka.util.Timeout
import de.garantiertnicht.supplyzepelin.Main
import de.garantiertnicht.supplyzepelin.bot.command.CommandExecution
import de.garantiertnicht.supplyzepelin.persistence.definitions._
import de.garantiertnicht.supplyzepelin.persistence.entities._
import de.garantiertnicht.supplyzepelin.persistence.entities.rewards.{Bribed, Collect, Event, Recurring}
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities._
import org.mongodb.scala.Observer
import org.mongodb.scala.result.UpdateResult

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Failure, Success}

case class Competition(
  message: Message,
  points: Long,
  tosLinkActivated: Boolean = false
) {
  def isForCompetition(reaction: GuildReaction): Boolean =
    reaction.reaction.getGuild.getSelfMember
      .hasPermission(
        reaction.reaction.getTextChannel,
        Permission.MESSAGE_HISTORY,
        Permission.MESSAGE_READ,
        Permission.MESSAGE_WRITE,
        Permission.MESSAGE_ADD_REACTION
      ) && reaction.reaction.getMessageIdLong == this.message.getIdLong &&
      reaction.reaction.getChannel.getIdLong == this.message.getChannel.getIdLong
}

case object GuildActorShouldBePoisoned

case class DropCrate(channel: TextChannel)

case class KillCompetition(competition: Competition)

case class PermissionCheck(time: Byte)

case class Bribe(member: Member, amount: Long)

case class Block(member: UserId)

case class Unblock(member: UserId)

case class GuildInfo(channel: TextChannel)

case class GetBlockedStatus(member: Member)

case class Transaction(amount: Points, to: Seq[Member], source: Event)

object RefundBribesAndKill

class GuildActor(guild: Guild) extends Actor {

  /** Users that were already recieved a penalty for clicking a reaction of a
    * semi-closed contest.
    */
  val usersCollectingCrate: mutable.Map[Member, Short] =
    mutable.Map[Member, Short]()

  /** Standard message to signify a crate has been dropped. */
  val lastCompetitionAwardMessageStandard = "The crate has been collected!"

  /** The logger to log to */
  val log = Logging(context.system, this)
  val blockedMembers = new mutable.ListBuffer[Long]()
  val PATTERN_REDBOT_TRANSFER_MESSAGE =
    "^([1-9][0-9]*) credits have been transferred to Supply Zeppelin's account\\.$".r
  val PATTERN_REDBOT_TRANSFER_COMMAND =
    s"^(.*)bank +transfer +<@!?${guild.getJDA.getSelfUser.getId}> +([0-9]+).*".r

  /** The current activity for that guild */
  var chatActivity: mutable.Map[Member, Int] = mutable.Map[Member, Int]()

  /** The last sender. Activity will only be granted for messages that have a
    * different sender.
    */
  var lastSender: Long = _

  /** Time the last competition ended as Linux Epoch */
  var lastTime: Long = System.currentTimeMillis() / 1000

  /** The currently active competition (in the state that clicking the reaction
    * will grant the reward)
    */
  var competitionStartedMillis = 0L
  var activeCompetition: Competition = _

  /** The competition that is in the state of users receiving penalties if they
    * click the reaction.
    */
  var lastCompetition: Competition = activeCompetition

  /** Timeout for asks */
  implicit val timeout = Timeout(1 minute)

  /** The dispatcher where ask-s will be executed. */
  implicit val dispatcher: ExecutionContextExecutor = context.system.dispatcher

  /**
    * The message that was created for the last competition. Will be null if
    * there is no message that is in the "click for penalty" state.
    */
  var lastCompetitionAwardMessage: String = lastCompetitionAwardMessageStandard

  /** This is the activity that is expected to be made within 10 minutes. */
  var standardActivity: Int = 20

  /** Counts the remaining drops for this guild until the score will be negative once. */
  var negativeCounter: Int = 2

  /** A list of all bribes collected */
  var bribes = new ListBuffer[Bribe]
  var bribesForCompetition = new ListBuffer[Bribe]

  /** If the guild should stop (mostly because ;stop) */
  var shouldStop = false
  var lastNetAdd = 0L

  context.setReceiveTimeout(15 minutes)

  override def receive: Receive = {
    case message: GuildMessage =>
      val now = System.currentTimeMillis() / 1000
      val diff = now - lastTime

      /* We increase the activity only if all of the below is true:
       * We can read, write, add our external emoji, read message history (to edit) and remove emotes (Manage
            Messages)
       * Either we should stop (;stop), or the last contest is more then 90s ago and the last sender is not the
            sender of this message.
       */
      if (message.message.getGuild.getSelfMember
          .hasPermission(
            message.message.getTextChannel,
            Permission.MESSAGE_READ,
            Permission.MESSAGE_EXT_EMOJI,
            Permission.MESSAGE_WRITE,
            Permission.MESSAGE_HISTORY,
            Permission.MESSAGE_ADD_REACTION,
            Permission.MESSAGE_MANAGE
          )
        && !isBlocked(message.message.getMember)
        && (
          shouldStop
          || (diff > 90 && message.message.getAuthor.getIdLong != lastSender)
        )) {
        // If for some reason we still have a competition open at this point, it shouldn't exist.
        if (lastCompetition != null) {
          killCompetition(KillCompetition(lastCompetition))
        }

        if (activeCompetition != null) {
          killCompetition(KillCompetition(activeCompetition))
        }

        // The activity is based on message count and mentions. The more you get mentioned, the more activity you get.
        val member = message.message.getMember
        chatActivity.put(member, chatActivity.getOrElse(member, 0) + 1)

        Option(message.message.getMessageReference)
          .filter(message2 => message.message.getIdLong != message2.getMessageIdLong)
          .flatMap(reference => Option(reference.getMessage))
          .foreach(message =>
            if (chatActivity.contains(message.getMember)) {
              chatActivity.put(message.getMember, chatActivity.getOrElse(member, 0) + 4)
            }
        )

        message.message.getMentionedMembers.forEach(member => {
          if (chatActivity.contains(member)) {
            chatActivity.put(member, chatActivity.getOrElse(member, 0) + 4)
          }
        })

        if ((chatActivity.nonEmpty && chatActivity.values.sum > standardActivity) || shouldStop) {
          self ! DropCrate(message.message.getTextChannel)
        }
      }
    case DropCrate(channel) ⇒
      if (guild.getSelfMember.hasPermission(
          channel,
          Permission.MESSAGE_READ,
          Permission.MESSAGE_EXT_EMOJI,
          Permission.MESSAGE_WRITE,
          Permission.MESSAGE_HISTORY,
          Permission.MESSAGE_ADD_REACTION,
          Permission.MESSAGE_MANAGE
        )) {
        val now = System.currentTimeMillis() / 1000
        val diff = now - lastTime

        if (activeCompetition == null) {
          lastTime = System.currentTimeMillis() / 1000

          // If the activity reaches the guessed activity for 10 minutes AND
          // there is no active competition, we drop a crate.
          startCompetition(channel, diff, chatActivity.toMap)

          // Report exact time for haxcident reporting
          competitionStartedMillis = System.currentTimeMillis()

          standardActivity = math
            .max(
              ((standardActivity * 600 / (diff
                .max(1) * (diff / 1200 + 1)) + standardActivity * 2) / 3).toInt,
              20
            )
            .min(2 << 16)
        }
      }
    case competition: Competition =>
      // Really open the competition.
      activeCompetition = competition
      // Time out after one minute.
      context.system.scheduler
        .scheduleOnce(1 minute, self, KillCompetition(competition))

      competition.message
        .addReaction(Main.reaction)
        .queue((_) ⇒ {
          competition.message.addReaction(Main.tosEmote).queue()
        }, (exception: Throwable) => {
          killCompetition(KillCompetition(competition))
        })
    case kill: KillCompetition =>
      killCompetition(kill) // Forcefully ends the competition
    case GuildActorShouldBePoisoned =>
      log.debug("Unloading…")

      // We unload the actor if we received no message on it after 15 minutes.
      // Can also occur on ;stop.
      if (activeCompetition != null) {
        // Kill the competition in the case of one being active
        self ! KillCompetition(activeCompetition)
      }

      // Only unload if there are bribes active.
      if ((bribes.isEmpty && activeCompetition == null) || Main.jda
          .getGuildById(guild.getId) == null) {
        self ! RefundBribesAndKill
      } else {
        shouldStop = true
        context.system.scheduler.scheduleOnce(50 seconds, self, RefundBribesAndKill)
      }
    case _: ReceiveTimeout =>
      // This is called once the guild is inactive for 15 minutes.
      if (bribes.isEmpty) {
        // We don't naturely unload if there are pending bribes as they are not persistent.
        context.stop(self)
      }
    case reaction: GuildReaction =>
      // Received if a reaction is added to a guild.
      // Only do stuff if member is not blocked
      if (!isBlocked(reaction.author)) {
        // Try to close competition
        closeCompetition(reaction)

        // Special Treatment for legal emotes
        if (reaction.reaction.getReactionEmote.isEmote &&
          reaction.reaction.getReactionEmote.getEmote == Main.tosEmote
        ) {
          val message = reaction.reaction.getChannel
            .retrieveMessageById(reaction.reaction.getMessageId)
            .complete()
          if (message != null &&
            message.getAuthor == Main.jda.getSelfUser &&
            !message.getContentRaw.contains(Main.tosNotice)) {
            val newContent =
              if (message.getContentRaw.contains(Main.tosClickEmote)) {
                message.getContentRaw
                  .replace(Main.tosClickEmote, Main.tosNotice)
              } else {
                message.getContentRaw + " " + Main.tosNotice
              }

            message.editMessage(newContent).queue()

            // If this message is a crate message: Update competition
            // Needed as message may be updated after this
            if (activeCompetition != null && message.getIdLong == activeCompetition.message.getIdLong) {
              activeCompetition =
                activeCompetition.copy(tosLinkActivated = true)
            }
          }
        }
      }
    case command: GuildCommand =>
      // This happens if the message starts with a `;` and the sender is not a bot.
      if (!isBlocked(command.message.getMember) && !shouldStop) {
        Main.bot ! CommandExecution(command, self)
      }
    case bribe: Bribe =>
      // Add a bribe.
      bribes += bribe
    case Block(member) =>
      // Add a user to the blocked list.
      if (!blockedMembers.contains(member)) {
        blockedMembers += member
      }
    case Unblock(member) =>
      // Remove a user from the blocked list.
      if (blockedMembers.contains(member)) {
        blockedMembers -= member
      }
    case GetBlockedStatus(member) ⇒
      sender ! isBlocked(member)
    case GuildInfo(channel) =>
      // Send the guild info in a channel.

      val activityText = chatActivity
        .map(entry ⇒ s"${entry._1.getUser.getId}:${entry._2}")
        .mkString(" ")

      channel
        .sendMessage(
          s"activity=$activityText\n" +
            s"standartActivity=$standardActivity\n" +
            s"timeoutDiff=${System.currentTimeMillis() / 1000 - lastTime}\n" +
            s"pendingBribeAmount=${bribesForCompetition.length + bribes.length}\n" +
            s"lastCompetition=$lastCompetition\n" +
            s"activeCompetition=$activeCompetition"
        )
        .queue()
    case Transaction(points, to, source) ⇒ modify(points, source, to: _*)
    case RefundBribesAndKill =>
      if (activeCompetition != null) killCompetition(
        KillCompetition(activeCompetition)
      )

      if (lastCompetition != null) killCompetition(
        KillCompetition(lastCompetition)
      )

      bribes.foreach {bribe =>
        modify(
          bribe.amount,
          Bribed(),
          bribe.member
        )
      }

      log.debug(s"Stopped.")
      context.stop(self)
  }

  def modify(amount: Points, source: Event, to: Member*): Unit = {
    val actualAmount =
      amount.min(Main.MAX_POINT_MODIFICATION).max(-Main.MAX_POINT_MODIFICATION)

    to.filter(member => !member.getUser.isBot && !isBlocked(member)).foreach {
      member ⇒
        new GetInfoForMember(member).mongoQuery.head().onSuccess {
          case null ⇒
            UpdateScore(member, Score(actualAmount)).mongoQuery
              .subscribe(new Observer[UpdateResult] {
                override def onError(e: Throwable) = e.printStackTrace()

                override def onComplete() = {}

                override def onNext(result: UpdateResult) = {}
              })
          case entry: ScoreboardEntry ⇒
            val score = entry.score.total
            val transfer = if (score >= 0) {
              val maxAmount = Main.MAX_POINTS - score
              math.min(actualAmount, maxAmount)
            } else {
              val maxAmount = Main.MAX_POINTS - score.abs
              math.max(actualAmount, -maxAmount)
            }

            UpdateScore(member, Score(transfer)).mongoQuery
              .subscribe(new Observer[UpdateResult] {
                override def onError(e: Throwable) = e.printStackTrace()

                override def onComplete() = {}

                override def onNext(result: UpdateResult) = {}
              })
        }
    }

    Main.bot ! Recalculate(guild, source, to)
  }

  def startCompetition(
    channel: TextChannel,
    diff: Long,
    chatActivity: Map[Member, Int]
  ): Unit = {
    if (activeCompetition != null) {
      if (System.currentTimeMillis - competitionStartedMillis < 70000) {
        return
      }
      killCompetition(KillCompetition(activeCompetition))
    }

    // Change the bribes to the new competition so no new ones can be added for this
    bribesForCompetition ++= bribes
    bribes.clear()

    // Calculate the points
    var points = Math.min(
      getPointsForCompetition(diff, standardActivity),
      Main.MAX_POINT_MODIFICATION - 1
    )

    // Swap sing to create negative crates now and then
    // This only triggers on decent activity
    if (negativeCounter <= 0 && standardActivity > 300) {
      // We trigger the point removal
      Main.bot ! Recalculate(guild, Recurring())

      def observer(name: String) = new Observer[Any] {
        override def onNext(result: Any): Unit = {}

        override def onError(e: Throwable): Unit = {}

        override def onComplete(): Unit = {}
      }

      RemoveScore(guild).mongoQuery.subscribe(observer("Remove empty scores"))
      RemoveEmpty(guild).mongoQuery.subscribe(observer("Remove empty entries"))

      // Also, negate points

      points = -points
      negativeCounter = standardActivity % 11 + 2

      // Check if the guild has a weak security policy regularly as some big guilds may never reload.
      if (hasElevatedPermissionsInGuild(guild)) {
        disableGuildAndNotifyOwnerSecurity(guild)
      }
    }

    negativeCounter -= 1

    // Check if bribes are present and tell that information
    val bribesPresent = if (bribesForCompetition.nonEmpty) {
      "+ bribes"
    } else {
      "with no additional `;bribe`s"
    }

    val pointsString = s"$points±${getActivityMaxBonus(points)}"

    // Send message
    channel
      .sendMessage(
        s"A supply crate with $pointsString points $bribesPresent has dropped! Click the reaction to open it!" +
          s" ${Main.tosClickEmote}"
      )
      .queue(message => self ! Competition(message, points))
  }

  /**
    * Returns how many points a crate should drop.
    *
    * @param diff The difference in secounds sience the last crate dropped.
    * @return
    */
  def getPointsForCompetition(diff: Long, standardActivity: Int): Long =
    Math
      .max(
        (1 - (diff / 1200.0)) * 100 * (standardActivity / 142.0),
        10 * (standardActivity / 142.0)
      )
      .toLong

  def getActivityMaxBonus(points: Long): Long = {
    val base = (points.abs * 0.65).toLong
    points.abs - base
  }

  def getActivityBonus(
    points: Long,
    chatActivity: Map[Member, Int],
    member: Member
  ): Long = {
    if (chatActivity.isEmpty) {
      return -getActivityMaxBonus(points)
    }

    val bonus = getActivityMaxBonus(points) * 2

    val max = chatActivity.values.max
    val value = chatActivity.getOrElse(member, 0) / max.toDouble

    val totalBonus = ((bonus * value) - bonus / 2).toLong

    if (points > 0) {
      totalBonus
    } else {
      -totalBonus
    }
  }

  def getBribeAmount(
    droppedAmount: Long,
    bribedAmount: Long,
    owner: Boolean
  ): Long = {
    val absoluteBribeValue = bribedAmount.abs

    val scale = 500000
    val minBonus = 0.30625

    val bonus =
      ((scale / (scale + absoluteBribeValue).toDouble) * 3.75).max(minBonus)
    val absoluteAddedValue = (bribedAmount * bonus).toLong
    val newMaximumValue = absoluteBribeValue + bribedAmount

    if (owner) {
      newMaximumValue
    } else {
      newMaximumValue / 7
    }
  }

  // TODO Split this up, it is a bit long
  def closeCompetition(reaction: GuildReaction): Unit = {
    if (reaction.reaction.getReactionEmote.isEmote &&
      reaction.reaction.getReactionEmote.getEmote == Main.reaction
    ) {
      val difference = System.currentTimeMillis() - competitionStartedMillis
      if (difference < 180 + Main.jda.getGatewayPing * 2) {
        Haxcident.report(
          reaction.author,
          "Clicked reaction too fast",
          false,
          Seq(
            s"Time difference: $difference ms",
            s"Bribes: ${if (bribesForCompetition.nonEmpty) {
              if (bribesForCompetition.exists(_.member == reaction.author)) {
                "the person"
              } else {
                "another person"
              }
            } else {
              "noone"
            }}",
            s"Activity: ${chatActivity.getOrElse(reaction.author, 0)}/${chatActivity.values.sum}"
          )
        )
      }

      if (activeCompetition != null && activeCompetition.isForCompetition(
          reaction
        )) {
        Main.bot ! CrateCollectionStatistic(difference, Main.jda.getGatewayPing, true)

        var collectedPoints = activeCompetition.points
        var bribesInvolved = if (bribesForCompetition.nonEmpty) {
          " with bribes being involved (o.0)"
        } else {
          ""
        }

        bribesForCompetition
          .groupBy(_.member)
          .map(bribes ⇒ {
            bribes._2.reduce((a, b) ⇒ a.copy(amount = a.amount + b.amount))
          })
          .foreach(bribe => {
            if (collectedPoints < Main.MAX_POINT_MODIFICATION) {
              collectedPoints += getBribeAmount(
                activeCompetition.points,
                bribe.amount,
                reaction.author == bribe.member
              )
            } else {
              collectedPoints = Main.MAX_POINT_MODIFICATION - 1
            }
          })

        collectedPoints = collectedPoints
          .min(Main.MAX_POINT_MODIFICATION - 1)
          .max(-Main.MAX_POINT_MODIFICATION + 1)

        lastCompetition = activeCompetition.copy(points = collectedPoints)

        collectedPoints += getActivityBonus(
          collectedPoints,
          chatActivity.toMap,
          reaction.author
        )

        modify(collectedPoints, Collect(), reaction.author)

        lastCompetitionAwardMessage =
          s"""${Bot
               .formatMember(reaction.author)} got $collectedPoints points from a
             |supply crate$bribesInvolved!
             |""".stripMargin.lines.mkString(" ")

        activeCompetition = null

        val leftovers = getLeftoversForCompetition(lastCompetition)
        val leftoversBonus = getActivityMaxBonus(leftovers)
        val leftoversString = s"$leftovers±$leftoversBonus"
        val tosNotice = if (lastCompetition.tosLinkActivated) {
          Main.tosNotice
        } else {
          Main.tosClickEmote
        }

        lastCompetition.message
          .editMessage(
            lastCompetitionAwardMessage +
              s" No worries, you still can get $leftoversString points by " +
              s"clicking the reaction. $tosNotice"
          )
          .queue()

        lastTime = System.currentTimeMillis() / 1000

        usersCollectingCrate += ((reaction.author, 0))
      } else if (lastCompetition != null && lastCompetition.isForCompetition(
          reaction
        )) {
        Main.bot ! CrateCollectionStatistic(difference, Main.jda.getGatewayPing, false)

        if (!usersCollectingCrate.contains(reaction.author)) {
          usersCollectingCrate += ((reaction.author, 0))
          val leftovers = getLeftoversForCompetition(lastCompetition)
          modify(
            leftovers + getActivityBonus(
              leftovers,
              chatActivity.toMap,
              reaction.author
            ),
            Collect(),
            reaction.author
          )
        } else {
          usersCollectingCrate.update(
            reaction.author,
            (usersCollectingCrate(reaction.author) + 1).toShort
          )
        }
      }
    }

    if (shouldStop) {
      // Only allow one collection when we need to stop
      self ! KillCompetition(lastCompetition)
    }
  }

  override def preStart(): Unit = {
    if (hasElevatedPermissionsInGuild(guild)) {
      // We don't want to be on guilds that have a "questionable security policy".
      disableGuildAndNotifyOwnerSecurity(guild)
    }

    // This may result in blocked users being able to do one command
    new GetBlockedMembersForGuild(guild).mongoQuery.toFuture().onSuccess {
      case list: Seq[ScoreboardEntry] =>
        list.foreach(entry ⇒ self ! Block(entry._id.userId))
    }
  }

  def disableGuildAndNotifyOwnerSecurity(guild: Guild): Unit = {
    guild.getOwner.getUser
      .openPrivateChannel()
      .queue(channel => {
        channel
          .sendMessage(s"""I have administrator permissions on your server
                          |${guild.getName} which is bad. It could allow me to go
                          |rouge on your server or allow persons who find out the Bot
                          |token to do the same. Due to security reasons stated
                          |above, the bot will not work until you fix these issues
                          |(it will check every 10 minutes)
                          |""".stripMargin.lines.mkString(" "))
          .queue()
      })

    context become stopping
    context.system.scheduler.scheduleOnce(10 minutes, self, PermissionCheck(0))

    log.info(s"Unsafe guild ${guild.getId} ${guild.getName}")
  }

  /**
    * This state means that no commands will be processed.
    * We can only close competitions (but not start them) or check if we still
    * have elevated permissions.
    */
  def stopping: Receive = {
    case PermissionCheck(time) =>
      if (!hasElevatedPermissionsInGuild(guild)) {
        context.unbecome()
      } else {
        if (time == 5) {
          self ! RefundBribesAndKill
        } else {
          context.system.scheduler
            .scheduleOnce(10 minutes, self, PermissionCheck((time + 1).toByte))
        }
      }
    case _ => // ignored
  }

  /**
    * This method checks if the bot has access to manage roles. This will
    *
    * @param guild The guild to check
    * @return If the bot has elevated permissions in this guild.
    */
  def hasElevatedPermissionsInGuild(guild: Guild): Boolean =
    guild.getSelfMember.hasPermission(Permission.ADMINISTRATOR)

  /**
    * Closes the competition.
    *
    * @param kill The competition to close.
    */
  def killCompetition(kill: KillCompetition): Unit = {
    if (kill.competition != null) {
      if (guild.getSelfMember.hasPermission(Permission.MESSAGE_MANAGE)) {
        kill.competition.message.clearReactions.queue()
      }

      if (kill.competition == activeCompetition) {
        // It timed out before someone collected the crate.
        activeCompetition = null
        kill.competition.message
          .editMessage("❌ The supply crate has rotten away.")
          .queue()

        // Add back bribes if not used
        bribes ++= bribesForCompetition
        bribesForCompetition.clear()
        chatActivity.clear()

        if (shouldStop) {
          // If we should stop, do it now
          self ! RefundBribesAndKill
        }
      } else if (kill.competition == lastCompetition) {
        // It timed out after the crate has been collected
        lastCompetition = null

        // Haxcident Reporting
        val chatActivityPrevious = chatActivity.clone
        usersCollectingCrate.foreach(collector => {
          val member = collector._1
          val timesCollected = collector._2
          val activity = chatActivityPrevious.getOrElse(member, 0)

          def report(
            member: Member,
            points: Option[Long],
            bribed: Boolean,
            bribesExist: Boolean,
            timesClicked: Short,
            activity: Int,
            totalActivity: Int
          ): Unit = {
            val bribedString = if (bribesExist) {
              if (bribed) {
                "the person"
              } else {
                "other persons"
              }
            } else {
              "noone"
            }
            Haxcident.report(
              member,
              "Suspicious collection of a crate",
              timesClicked >= 2 && points.exists(_ > 1000),
              Seq(
                s"Points: ${points.getOrElse("Unknown")}",
                s"Bribed by: $bribedString",
                s"Clicked: ${timesClicked + 1} times",
                s"Activity: $activity/$totalActivity"
              )
            )
          }

          if (activity == 0 || timesCollected > 1) {
            val bribed = bribesForCompetition.exists(_.member == member)
            val bribesExist = bribesForCompetition.nonEmpty
            val totalActivity = chatActivityPrevious.values.sum

            new GetInfoForMember(member).mongoQuery.head().onComplete {
              case Success(info) ⇒
                if (info == null) {
                  report(
                    member,
                    None,
                    bribed,
                    bribesExist,
                    timesCollected,
                    activity,
                    totalActivity
                  )
                } else {
                  val points = info.score.total
                  report(
                    member,
                    Some(points),
                    bribed,
                    bribesExist,
                    timesCollected,
                    activity,
                    totalActivity
                  )
                }
              case Failure(e) =>
                report(
                  member,
                  None,
                  bribed,
                  bribesExist,
                  timesCollected,
                  activity,
                  totalActivity
                )
                e.printStackTrace()
            }
          }
        })

        kill.competition.message
          .editMessage(lastCompetitionAwardMessage)
          .queue()
        lastCompetitionAwardMessage = lastCompetitionAwardMessageStandard
        usersCollectingCrate.clear()
        bribesForCompetition.clear()
        chatActivity.clear()

        if (shouldStop && bribes.isEmpty) {
          // If we should stop, do it now
          self ! RefundBribesAndKill
        }

        Main.bot ! Recalculate(guild, Collect())
      }
    }
  }

  /**
    * Gets the penalty for a competition if someone clicks the reaction after
    * the crate has been collected.
    *
    * @param competition The competition to check for.
    * @return The penalty
    */
  def getLeftoversForCompetition(competition: Competition): Long = {
    val points = math.pow(competition.points.abs * 1.5, 0.75).toLong

    if (competition.points < 0) {
      -points
    } else {
      points
    }
  }

  /**
    * Checks if a user is blocked.
    *
    * @param member The member to check
    * @return True if the member is blocked, false otherwise.
    */
  def isBlocked(member: Member): Boolean =
    blockedMembers.contains(member.getUser.getIdLong) ||
      Bot.isBlocked(member.getUser)
}
