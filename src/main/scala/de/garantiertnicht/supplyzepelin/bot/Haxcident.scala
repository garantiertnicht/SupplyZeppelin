/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot

import de.garantiertnicht.supplyzepelin.Main
import de.garantiertnicht.supplyzepelin.persistence.Connection
import net.dv8tion.jda.api.entities.{Guild, Member, User}
import org.mongodb.scala.model.Filters._

import scala.concurrent.ExecutionContext
import scala.util.Success

object Haxcident {
  val channel = Main.haxcidentChannel

  def report(
    member: Member,
    reason: String,
    mention: Boolean,
    additionalInfo: Seq[String] = Seq()
  )(implicit executionContext: ExecutionContext): Unit =
    reportUser(
      member.getUser,
      member.getGuild,
      reason,
      mention,
      additionalInfo ++ Seq(
        "----",
        s"Permissions: ${member.getPermissions().toArray.mkString(", ")}",
        s"Owner: ${member.isOwner}",
        s"Member since: ${member.getTimeJoined.toString}"
      )
    )

  def reportUser(
    user: User,
    guild: Guild,
    reason: String,
    mention: Boolean,
    additionalInfo: Seq[String] = Seq()
  )(implicit executionContext: ExecutionContext): Unit = {
    val string = new StringBuilder

    def write(line: String) = {
      string.append(line)
      string.append('\n')
    }

    def send(string: String) = {
      val mentionString = if (mention) "@everyone" else ""
      channel.sendMessage(s"$mentionString```$string```").queue()
    }

    write("**Haxcident**")
    write(s"User: ${user.getIdLong} ${user.getName}#${user.getDiscriminator}")
    write(s"Server: ${guild.getId} ${guild.getName}")
    write(s"Account created: ${user.getTimeCreated}")
    write(s"Server created: ${user.getTimeCreated}")

    write("----")
    write(reason)

    if (reason.nonEmpty) {
      write("----")
      additionalInfo.foreach(write)
    }

    write("----")
    write(s"Common Servers: ${user.getMutualGuilds.size()}")

    Connection.scoreboard
      .find(equal("_id.userId", user.getIdLong))
      .toFuture()
      .onComplete {
        case Success(entries) =>
          val positive = entries.count(_.score.total > 0)
          val negative = entries.count(_.score.total < 0)
          val neutral = entries.count(_.score.total == 0)

          val blocked = entries.count(_.blocked.contains(true))

          write(s"Positive: $positive")
          write(s"Neutral: $neutral")
          write(s"Negative: $negative")
          write(s"Blocked: $blocked")

          send(string.mkString)
        case _ ⇒
          send(string.mkString)
      }
  }
}
