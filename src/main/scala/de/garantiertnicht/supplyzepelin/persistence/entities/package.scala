/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.persistence

import de.garantiertnicht.supplyzepelin.Main
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.entities._

import scala.language.implicitConversions

package object entities {
  type Points = Long

  type GuildId = Long
  type RoleId = Long
  type UserId = Long
  type TextChannelId = Long

  def guildFromId(guild: GuildId, jda: JDA = Main.jda): Option[Guild] =
    Option(jda.getGuildById(guild))

  def roleFromId(role: RoleId, jda: JDA = Main.jda): Option[Role] =
    Option(jda.getRoleById(role))

  def userFromId(user: UserId, jda: JDA = Main.jda): Option[User] =
    Option(jda.getUserById(user))

  def textChannelFromId(
    channel: TextChannelId,
    jda: JDA = Main.jda
  ): Option[TextChannel] =
    Option(jda.getTextChannelById(channel))

  implicit def id2guild(id: GuildId): Option[Guild] = guildFromId(id)

  implicit def id2role(id: RoleId): Option[Role] = roleFromId(id)

  implicit def id2user(id: UserId): Option[User] = userFromId(id)

  implicit def id2textChannel(id: TextChannelId): Option[TextChannel] =
    textChannelFromId(id)

  implicit def guild2id(guild: Guild): GuildId = guild.getIdLong

  implicit def role2id(role: Role): RoleId = role.getIdLong

  implicit def user2id(user: User): UserId = user.getIdLong

  implicit def textChannel2id(textChannel: TextChannel): TextChannelId =
    textChannel.getIdLong
}
