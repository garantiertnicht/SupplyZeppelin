package de.garantiertnicht.supplyzepelin.bot.command.management

import akka.actor.ActorRef
import de.garantiertnicht.supplyzepelin.{Main}
import de.garantiertnicht.supplyzepelin.bot.{Bot, GloballyUnblock, Haxcident}
import de.garantiertnicht.supplyzepelin.bot.command.Command
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.Permission

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext

object GlobalUnblock extends Command {
  override val name: String = "downgrade"

  override def execute(
    commandAndArgs: Array[String],
    originalMessage: Message,
    guild: ActorRef
  )(implicit ex: ExecutionContext): Unit = {
    val member = Option(
      Haxcident.channel.getGuild.getMember(originalMessage.getMember.getUser)
    )

    if (!member.exists(
        _.hasPermission(Haxcident.channel, Permission.MESSAGE_MANAGE)
      )) {
      return
    }

    if (originalMessage.getAuthor.getIdLong != Main.superUser) {
      if (originalMessage.getMentionedUsers.asScala.toStream
          .filter(!_.isBot)
          .map(Bot.getBlock)
          .exists(_.exists(_.from != originalMessage.getAuthor.getIdLong))) {
        originalMessage.getChannel
          .sendMessage("You can only downgrade users which you upgraded.")
          .queue()

        return
      }
    }

    originalMessage.getMentionedUsers.asScala.filter(!_.isBot).foreach(user => {
      Main.bot ! GloballyUnblock(user)
      Haxcident.reportUser(
        user,
        originalMessage.getGuild,
        "Got Downgraded from Supply Boat",
        true,
        Seq(
          s"Staff Member: ${originalMessage.getMember.getUser.getName}#" +
            s"${originalMessage.getMember.getUser.getDiscriminator}"
        )
      )
    })
    originalMessage.getChannel
      .sendMessage(
        "Sorry, but you are no longer eligible for Supply Boat and have been refunded."
      )
      .queue()
  }
}
