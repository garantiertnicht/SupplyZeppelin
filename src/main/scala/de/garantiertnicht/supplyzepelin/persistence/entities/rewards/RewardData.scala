/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.persistence.entities.rewards

import de.garantiertnicht.supplyzepelin.bot.utils.RankedMember
import de.garantiertnicht.supplyzepelin.persistence.Connection
import de.garantiertnicht.supplyzepelin.persistence.entities.GuildId
import net.dv8tion.jda.api.entities.{Guild, Member}
import org.mongodb.scala.model.Filters._

import scala.concurrent.Future
import scala.language.implicitConversions

case class RewardIdentifier(guildId: GuildId, name: String)

case class Reward(
  _id: RewardIdentifier,
  data: RewardData,
  options: Option[RewardOptions]
) {
  def merge(reward: Reward) = Reward(_id, data.merge(reward.data), options)

  def squash =
    data.squash.map((squashed: RewardData) ⇒ Reward(_id, squashed, options))

  def applyReward(member: Member) = data.applyReward(member)

  def reverse = Reward(_id, data.reverse, options)

  def requirementsMet(member: RankedMember, source: Event, present: Boolean) =
    data.requirementsMet(member, source, present)

  override def toString = {
    val options = if (this.options.isDefined) {
      s" SET (${this.options.get})"
    } else {
      ""
    }

    s"${_id.name} $data$options"
  }

  override def equals(obj: scala.Any): Boolean = obj match {
    case other: Reward ⇒ _id == other._id
    case _ ⇒ false
  }

  def onlyDirty: Reward = this.copy(data = data.onlyDirty)

  def isValid(guild: Guild): Seq[String] = data.isValid(guild)
}

object Reward {
  def getRewardFromIdentifier(
    rewardIdentifier: RewardIdentifier
  ): Future[Reward] =
    Connection.rewards.find(equal("_id", rewardIdentifier)).head()

  implicit def id2reward(rewardIdentifier: RewardIdentifier): Future[Reward] =
    getRewardFromIdentifier(rewardIdentifier)
}

case class RewardData(requires: Seq[RewardEntity], grants: Seq[RewardEntity]) {
  def applyReward(member: Member) =
    grants.foreach(_.applyReward(member))

  def reverse =
    RewardData(requires, grants.map(_.reverse))

  def requirementsMet(member: RankedMember, source: Event, present: Boolean) =
    !requires.exists(!_.requirementsMet(member, source, present))

  def merge(other: RewardData): RewardData =
    RewardData(requires ++: other.requires, grants ++: other.grants)

  def squash: Option[RewardData] = {
    val newRequires = requires
      .filter(_.isRequirement)
      .groupBy(_.getClass.getCanonicalName)
      .map(pair ⇒ pair._2.reduce((a, b) ⇒ a.merge(b)))
      .map(_.squash)
      .filter(_.isDefined)
      .map(_.get)
      .toSeq
    val newGrants = grants
      .filter(_.isReward)
      .groupBy(_.getClass.getCanonicalName)
      .map(pair ⇒ pair._2.reduceLeft((a, b) ⇒ a.merge(b)))
      .map(_.squash)
      .filter(_.isDefined)
      .map(_.get)
      .toSeq

    if (newGrants.nonEmpty && newRequires.nonEmpty) {
      Some(RewardData(newRequires, newGrants))
    } else {
      None
    }
  }

  def onlyDirty: RewardData = copy(requires, grants.filter(_.dirty))

  def isValid(guild: Guild): Seq[String] =
    (
      requires.map(
        requirement =>
          requirement.isValidRequirement(guild).map(a => s"`$requirement`: $a")
      ) ++
        grants.map(
          grant => grant.isValidReward(guild).map(a => s"`$grant`: $a")
        )
    ).filter(_.isDefined).map(_.get)

  override def toString =
    s"REQUIRES [ ${requires.mkString(", ")} ] GRANTS [ ${grants.mkString(", ")} ]"
}
