/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.persistence

import akka.Done
import akka.actor.CoordinatedShutdown
import com.mongodb.connection.{ClusterSettings, ConnectionPoolSettings}
import de.garantiertnicht.supplyzepelin.Main
import de.garantiertnicht.supplyzepelin.persistence.entities._
import de.garantiertnicht.supplyzepelin.persistence.entities.rewards._
import de.garantiertnicht.supplyzepelin.Main.{jda, system}
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.bson.codecs.{DEFAULT_CODEC_REGISTRY, Macros}
import org.mongodb.scala.{MongoClient, MongoClientSettings, MongoCollection, MongoCredential, ServerAddress}

import scala.collection.JavaConverters._
import scala.concurrent.Future
import scala.util.Success
import scala.concurrent.ExecutionContext.Implicits.global

object Connection {
  private val mongoSettings = MongoClientSettings
    .builder()
    .clusterSettings(
      ClusterSettings
        .builder()
        .hosts(List(ServerAddress("localhost")).asJava)
        .maxWaitQueueSize(Int.MaxValue)
        .build()
    )
    .credentialList(
      List(
        MongoCredential.createCredential(
          "supplyzeppelin",
          "admin",
          Main.databasePassword.toCharArray
        )
      ).asJava
    )
    .connectionPoolSettings(
      ConnectionPoolSettings.builder().maxSize(100).build()
    )
    .build()

  private val mongo = MongoClient(mongoSettings)

  CoordinatedShutdown(Main.system).addTask(
    CoordinatedShutdown.PhaseServiceStop,
    "Closing Database Connection"
  ) {() =>
    Future {
      mongo.close()
      Done
    }
  }

  private val db = mongo
    .getDatabase(Main.databaseCollection)
    .withCodecRegistry(
      fromRegistries(
        fromProviders(
          Macros.createCodecProviderIgnoreNone[MemberInfo](),
          Macros.createCodecProviderIgnoreNone[Score](),
          Macros.createCodecProviderIgnoreNone[ScoreboardEntry](),
          Macros.createCodecProviderIgnoreNone[RewardData](),
          Macros.createCodecProviderIgnoreNone[RewardIdentifier](),
          Macros.createCodecProviderIgnoreNone[RewardOptions](),
          Macros.createCodecProviderIgnoreNone[RewardEntity](),
          Macros.createCodecProviderIgnoreNone[Event](),
          Macros.createCodecProviderIgnoreNone[RewardInfo](),
          Macros.createCodecProviderIgnoreNone[Reward](),
          Macros.createCodecProviderIgnoreNone[GlobalBlock](),
          Macros.createCodecProviderIgnoreNone[Honorific]()
        ),
        DEFAULT_CODEC_REGISTRY
      )
    )

  var _scoreboard: MongoCollection[ScoreboardEntry] = _
  var _rewards: MongoCollection[Reward] = _
  var _blocks: MongoCollection[GlobalBlock] = _
  var _honorifics: MongoCollection[Honorific] = _

  implicit val ec = scala.concurrent.ExecutionContext.global

  db.listCollectionNames().toFuture().onComplete {
    case Success(collections) =>
      if (!collections.contains("scoreboard")) {
        db.createCollection("scoreboard")
      }
      if (!collections.contains("rewards")) {
        db.createCollection("rewards")
      }
      if (!collections.contains("blocks")) {
        db.createCollection("blocks")
      }
      if (!collections.contains("honorifics")) {
        db.createCollection("honorifics")
      }

      _scoreboard = db.getCollection("scoreboard")
      _rewards = db.getCollection("rewards")
      _blocks = db.getCollection("blocks")
      _honorifics = db.getCollection("honorifics")

      println("Database connection is now ready.")
  }

  def scoreboard = _scoreboard

  def rewards = _rewards

  def blocks = _blocks

  def honorifics = _honorifics

  def waitUntilReady() =
    while (scoreboard == null || _rewards == null) Thread.sleep(100)
}
