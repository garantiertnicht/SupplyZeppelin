package de.garantiertnicht.supplyzepelin.persistence.entities

case class Honorific(_id: Long, honorific: String)
