/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.command.fun

import akka.actor.ActorRef
import de.garantiertnicht.supplyzepelin.bot.{Bot, RewardActor}
import de.garantiertnicht.supplyzepelin.bot.command.Command
import de.garantiertnicht.supplyzepelin.bot.utils.RankedMember
import de.garantiertnicht.supplyzepelin.persistence.definitions.{
  AddRewardToMember,
  FindReward,
  UpdateScore
}
import de.garantiertnicht.supplyzepelin.persistence.entities.rewards.{
  Reward,
  RewardIdentifier
}
import de.garantiertnicht.supplyzepelin.persistence.entities.{rewards, Score}
import net.dv8tion.jda.api.entities.Message

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

object Redeem extends Command {
  override val name: String = "redeem"

  override def execute(
    commandAndArgs: Array[String],
    originalMessage: Message,
    guild: ActorRef
  )(implicit ex: ExecutionContext): Unit = {
    val rewardName =
      commandAndArgs.slice(1, commandAndArgs.length).mkString(" ").toUpperCase
    val identifier =
      RewardIdentifier(originalMessage.getGuild.getIdLong, rewardName)

    new FindReward(identifier).mongoQuery.head().onComplete {
      case Success(reward: Reward) ⇒
        if (!reward.options.enabled) {
          originalMessage.getChannel
            .sendMessage(
              s"${Bot.formatMember(originalMessage.getMember)} This reward is disabled."
            )
            .queue()
          return
        }

        val ranked = RewardActor
          .mapMembersToRankedMembers(
            originalMessage.getGuild.getMembers.asScala,
            originalMessage.getGuild
          )
          .find(
            _.entry._id.userId == originalMessage.getMember.getUser.getIdLong
          )

        ranked match {
          case Some(ranked: RankedMember) ⇒
            if (ranked.entry.rewards.granted.contains(reward._id.name)) {
              originalMessage.getChannel
                .sendMessage(
                  s"${Bot.formatMember(originalMessage.getMember)} has already redeemed this reward!"
                )
                .queue()
            } else {
              if (!reward.requirementsMet(ranked, rewards.Redeem(), false)) {
                originalMessage.getChannel
                  .sendMessage(
                    s"${Bot.formatMember(originalMessage.getMember)} does not meet all requirements for this reward!"
                  )
                  .queue()
              } else {
                if (reward.options.persist) {
                  new AddRewardToMember(reward, originalMessage.getMember).mongoQuery
                    .head()
                    .onComplete {
                      case Success(_) ⇒
                        originalMessage.getChannel
                          .sendMessage(
                            s"${Bot.formatMember(originalMessage.getMember)} has redeemed a reward!"
                          )
                          .queue()
                        reward.applyReward(originalMessage.getMember)
                      case Failure(exception) ⇒
                        originalMessage.getChannel
                          .sendMessage(
                            s"Sorry, but I messed things up, ${Bot.formatMember(originalMessage.getMember)}. Try again later."
                          )
                          .queue()
                        exception.printStackTrace()
                    }
                } else {
                  originalMessage.getChannel
                    .sendMessage(
                      s"${Bot.formatMember(originalMessage.getMember)} has redeemed a reward!"
                    )
                    .queue()
                  reward.applyReward(originalMessage.getMember)
                }
              }
            }
          case None ⇒
            // We need to create a ranked member first, so we create a score of 0
            UpdateScore(originalMessage.getMember, Score(0)).mongoQuery
              .head()
              .onComplete {
                case Success(_) ⇒
                  execute(commandAndArgs, originalMessage, guild)
                case Failure(exception) ⇒
                  originalMessage.getChannel
                    .sendMessage(
                      s"Sorry, but I messed things up, ${Bot.formatMember(originalMessage.getMember)}. Try again later."
                    )
                    .queue()
                  exception.printStackTrace()
              }
        }
      case Success(null) ⇒
        originalMessage.getChannel
          .sendMessage(
            s"There is no such reward, ${Bot.formatMember(originalMessage.getMember)}!"
          )
          .queue()
      case Failure(exception) ⇒
        originalMessage.getChannel
          .sendMessage(
            s"Sorry, but I messed things up, ${Bot.formatMember(originalMessage.getMember)}. Try again later."
          )
          .queue()
        exception.printStackTrace()
    }
  }
}
