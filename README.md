# Supply Zeppelin
## About
Supply Zeppelin is a bot that will drop crates with points if chat is active. The first one that clicks the following reaction will get the points.
Peoples can also check a per-guild scoreboard. For more help, check ;help.

I do host an instance which you can [add to your guild](https://discordapp.com/oauth2/authorize?client_id=317248850672746496&scope=bot&permissions=355392).

## Programming
Don't. This code is an utter mess.

If you still wish to run it, copy the `settings.properties.sample` file to
`settings.properties` and fill out accordingly. If you make code changes, change
the link to the source code in `@mention help`.

If you find some hardcoded thing that should go into the properties, don't shy
away from a Merge Request or just pointing it out.