/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.persistence.definitions

import de.garantiertnicht.supplyzepelin.persistence.Connection
import de.garantiertnicht.supplyzepelin.persistence.entities.{rewards, GuildId}
import de.garantiertnicht.supplyzepelin.persistence.entities.rewards.{
  MultiplyScore,
  Recurring,
  Reward,
  RewardData,
  RewardIdentifier,
  RewardOptions,
  RewardSource,
  ScoreAmount
}
import org.mongodb.scala.FindObservable
import org.mongodb.scala.model.Filters._

class GetRewards(guild: GuildId) extends MongoQuery[Reward] {
  override val mongoQuery: FindObservable[Reward] =
    Connection.rewards.find(equal("_id.guildId", guild.toLong))

  def withManaged(all: Seq[Reward]): Seq[Reward] = {

    var mut = all.toBuffer

    GetRewards
      .managed(guild)
      .foreach(reward => {
        if (!mut.exists(_._id.name == reward._id.name)) {
          mut += reward
        }
      })

    mut
  }
}

object GetRewards {
  def managed(guild: GuildId): Seq[Reward] = Seq(
    Reward(
      RewardIdentifier(guild, "⋄DECAY"),
      RewardData(
        Seq(RewardSource(Recurring()), ScoreAmount(1, 0)),
        Seq(MultiplyScore(0.995))
      ),
      Some(RewardOptions(Some(Int.MaxValue), Some(-1), Some(true), None))
    )
  )
}
