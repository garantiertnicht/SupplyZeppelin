name := "Supply Zeppelin"

version := "1.0"

scalaVersion := "2.12.2"

resolvers += MavenRepository("m2-dv8tion", "https://m2.dv8tion.net/releases")
resolvers += Resolver.jcenterRepo

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.6.10",
  "com.typesafe.akka" %% "akka-slf4j" % "2.6.10",
  "net.dv8tion" % "JDA" % "4.4.0_352",
  "org.mongodb.scala" %% "mongo-scala-driver" % "2.1.0",
  "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.6",
  "org.apache.commons" % "commons-text" % "1.1"
)
