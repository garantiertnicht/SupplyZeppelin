package de.garantiertnicht.supplyzepelin.bot.command.management

import akka.actor.ActorRef
import de.garantiertnicht.supplyzepelin.bot.command.Command
import de.garantiertnicht.supplyzepelin.bot.{Bot, Haxcident, UpdateHonorific}
import de.garantiertnicht.supplyzepelin.Main
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.Permission

import scala.concurrent.ExecutionContext

object Honorific extends Command {
  override val name: String = "honorific"

  override def execute(
    commandAndArgs: Array[String],
    originalMessage: Message,
    guild: ActorRef
  )(implicit ex: ExecutionContext): Unit = {
    val member = Option(
      Haxcident.channel.getGuild.getMember(originalMessage.getMember.getUser)
    )

    if (!member.exists(
        _.hasPermission(Haxcident.channel, Permission.MESSAGE_MANAGE)
      )) {
      return
    }

    if (commandAndArgs.length < 2 ||
      originalMessage.getMentionedUsers.size() != 2 ||
      !commandAndArgs(1).matches("<@!?[0-9]+>")) {

      originalMessage.getChannel
        .sendMessage("Usage: ;honorific @user [honorific]")
        .queue()

      return
    }

    val user = originalMessage.getMentionedUsers.get(1)
    val honorific =
      if (commandAndArgs.length == 2) ""
      else commandAndArgs.drop(2).mkString(" ")

    Main.bot ! UpdateHonorific(user, honorific)

    originalMessage.getChannel
      .sendMessage(
        s"Updated Honorific for " +
          (if (honorific.isEmpty) "" else s"_${honorific}_ ") + user.getAsMention
      )
      .queue()
    Haxcident.report(
      originalMessage.getMember,
      "Updated Honorific",
      originalMessage.getAuthor.getIdLong != user.getIdLong,
      Seq(s"Target: ${Bot.formatUser(user, false)}")
    )
  }
}
