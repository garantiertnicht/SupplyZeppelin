/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.persistence.entities.rewards

sealed class Event

case class Redeem() extends Event {
  override def toString: String = "REDEEM"
}

case class PointTransfer() extends Event {
  override def toString: String = "TRANSFER"
}

case class Collect() extends Event {
  override def toString: String = "COLLECT"
}

case class Recurring() extends Event {
  override def toString: String = "RECURRING"
}

case class Bribed() extends Event {
  override def toString: String = "BRIBED"
}

case class RewardModified() extends Event {
  override def toString: String = "REWARD_MODIFIED"
}

case class MemberUpdate() extends Event {
  override def toString: String = "MEMBER_UPDATE"
}

case class Nothing() extends Event
