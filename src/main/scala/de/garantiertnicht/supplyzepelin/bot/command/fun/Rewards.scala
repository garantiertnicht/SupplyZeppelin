/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.command.fun

import akka.actor.ActorRef
import de.garantiertnicht.supplyzepelin.bot.command.Command
import de.garantiertnicht.supplyzepelin.bot.{deployment, Bot}
import de.garantiertnicht.supplyzepelin.bot.deployment.Deployment
import de.garantiertnicht.supplyzepelin.persistence.definitions.{
  FindReward,
  GetInfoForMember,
  GetRewards
}
import de.garantiertnicht.supplyzepelin.persistence.entities.rewards.RewardIdentifier
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Message

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

object Rewards extends Command {
  override val name: String = "rewards"
  override val canary: Option[Deployment] = Some(deployment.Rewards)

  val PAGE_LIMIT: Int = 3

  override def execute(
    commandAndArgs: Array[String],
    originalMessage: Message,
    guild: ActorRef
  )(implicit ex: ExecutionContext): Unit =
    if (commandAndArgs.length > 1 && !commandAndArgs(1).startsWith("<@")) {
      executeDetails(commandAndArgs, originalMessage, guild)
    } else {
      executeList(commandAndArgs, originalMessage, guild)
    }

  def executeDetails(
    commandAndArgs: Array[String],
    originalMessage: Message,
    guild: ActorRef
  )(implicit ex: ExecutionContext): Unit = {
    val rewardName =
      commandAndArgs.slice(1, commandAndArgs.length).mkString(" ").toUpperCase
    val rewardIdentifier =
      RewardIdentifier(originalMessage.getGuild.getIdLong, rewardName)
    new FindReward(rewardIdentifier).mongoQuery.head().onComplete {
      case Success(null) =>
        val managed = GetRewards
          .managed(originalMessage.getGuild.getIdLong)
          .find(_._id == rewardIdentifier)

        if (managed.isDefined) {
          originalMessage.getChannel
            .sendMessage(
              s"${Bot.formatMember(originalMessage.getMember)}: ```\n" +
                s"${managed.get}```\n" +
                "This is a managed reward provided by garantiertnicht software. " +
                "You cannot modify it, but create it. If you wish to do so, " +
                "remove the MANAGED option."
            )
            .queue()

          return
        }

        originalMessage.getChannel
          .sendMessage(
            s"No such reward, ${Bot.formatMember(originalMessage.getMember)}!"
          )
          .queue()
      case Success(reward) =>
        originalMessage.getChannel
          .sendMessage(
            s"${Bot.formatMember(originalMessage.getMember)}: ```\n" +
              s"$reward```"
          )
          .queue()
      case Failure(_) ⇒
        originalMessage.getChannel
          .sendMessage(
            s"Sorry, but the database refused to answer. Please try again later."
          )
          .queue()
    }
  }

  def executeList(
    commandAndArgs: Array[String],
    originalMessage: Message,
    guild: ActorRef
  )(implicit ex: ExecutionContext): Unit = {
    val member = originalMessage.getMentionedUsers.asScala.filter(!_.isBot).headOption
      .map(user ⇒ originalMessage.getGuild.getMember(user))
      .getOrElse(originalMessage.getMember)

    val getRewards = new GetRewards(originalMessage.getGuild.getIdLong)
    val allRewards =
      getRewards.mongoQuery.toFuture()
    val ownRewards = new GetInfoForMember(member).mongoQuery.head()
    val page = try {
      commandAndArgs.headOption.map(_.toInt).getOrElse(1)
    } catch {
      case _: NumberFormatException ⇒ 1
    }

    allRewards.zip(ownRewards).onComplete {
      case Success((allWithoutManaged, scoreboard)) =>
        val all = getRewards.withManaged(allWithoutManaged).sortBy(_._id.name)

        val mine = if (scoreboard != null && scoreboard.rewards.nonEmpty) {
          all.filter(
            reward ⇒ scoreboard.rewards.granted.contains(reward._id.name)
          )
        } else {
          Seq()
        }

        val other = all.diff(mine)

        val embed = if (all.nonEmpty) {
          new EmbedBuilder()
            .setTitle(
              s"Rewards of ${member.getEffectiveName}#${member.getUser.getDiscriminator}"
            )
            .setDescription(mine.map(_._id.name).mkString("\n"))
            .addField(
              "Other Rewards",
              other.map(_._id.name).mkString("\n"),
              false
            )
            .setFooter(
              "You can also execute ;rewards <name> to get the definition of a reward or ;rewards <@mention>" +
                "to check rewards of other users.",
              null
            )
            .build()
        } else {
          new EmbedBuilder()
            .setTitle(s"The space aliens feel lonely!")
            .setDescription(
              "This server does not have any rewards yet. Admins can create some with ;reward tho! Read" +
                " up on help on how to do it."
            )
            .build()
        }

        originalMessage.getChannel.sendMessage(embed).queue()
    }
  }
}
