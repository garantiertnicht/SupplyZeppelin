/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.command.management

import akka.actor.ActorRef
import de.garantiertnicht.supplyzepelin.Main
import de.garantiertnicht.supplyzepelin.bot.command.Command
import de.garantiertnicht.supplyzepelin.bot.command.parsing.RewardParser
import de.garantiertnicht.supplyzepelin.bot.deployment.{Deployment, Rewards}
import de.garantiertnicht.supplyzepelin.bot.{
  Bot,
  Haxcident,
  Recalculate,
  RemoveRewardFromMembers
}
import de.garantiertnicht.supplyzepelin.persistence.definitions.{GetRewards, _}
import de.garantiertnicht.supplyzepelin.persistence.entities.rewards.{
  Reward,
  RewardData,
  RewardIdentifier,
  RewardModified
}
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.Message

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success, Try}

object Reward extends Command {
  override val name = "reward"
  override val permission = Some(Permission.ADMINISTRATOR)
  override val canary: Option[Deployment] = Some(Rewards)

  override def execute(
    commandAndArgs: Array[String],
    originalMessage: Message,
    guild: ActorRef
  )(implicit ex: ExecutionContext): Unit = {
    if (commandAndArgs.length < 3) {
      originalMessage.getChannel
        .sendMessage(
          s"${Bot.formatMember(originalMessage.getMember)} Usage: ;reward <create|modify|delete> <definition>"
        )
        .queue()
      return
    }

    val definition = commandAndArgs
      .slice(2, commandAndArgs.length)
      .mkString(" ")
      .toUpperCase
      .replace('\n', ' ')
    commandAndArgs(1).toLowerCase match {
      case "create" | "add" | "new" ⇒
        val parser = new RewardParser(originalMessage.getGuild)
        parser.parse(parser.oneReward(), definition + parser.END) match {
          case parser.Success(fullReward: Reward, _) ⇒
            val errors = fullReward.isValid(originalMessage.getGuild)
            if (errors.nonEmpty) {
              originalMessage.getChannel
                .sendMessage(
                  s"${Bot.formatMember(originalMessage.getMember)} Validation Errors:\n" +
                    errors.mkString("\n")
                )
                .queue()
              return
            }

            fullReward.squash match {
              case Some(reward) ⇒
                new RewardCount(reward._id).mongoQuery.head().onComplete {
                  case Success(false) =>
                    new AddReward(reward).mongoQuery
                      .head()
                      .onComplete({
                        case Success(_) ⇒
                          originalMessage.getChannel
                            .sendMessage(
                              s"${Bot.formatMember(originalMessage.getMember)} added a reward with the definition `$reward`!"
                            )
                            .queue()
                          Main.bot ! Recalculate(
                            originalMessage.getGuild,
                            RewardModified()
                          )

                          new GetRewards(originalMessage.getGuild.getIdLong).mongoQuery
                            .toFuture()
                            .onComplete {
                              case Success(list) =>
                                if (list.length >= 20) {
                                  Haxcident.report(
                                    originalMessage.getMember,
                                    "Suspicious Amount of Rewards",
                                    true,
                                    Seq(
                                      s"Amount: ${list.length}",
                                      s"Rewards: ${list.map(_._id.name).mkString(", ").take(500).mkString}"
                                    )
                                  )
                                }
                              case Failure(e) ⇒
                                e.printStackTrace()
                            }

                        case Failure(e) => e.printStackTrace()
                      })
                  case Success(true) =>
                    originalMessage.getChannel
                      .sendMessage(
                        s"There is already a reward with that name, ${Bot
                          .formatMember(originalMessage.getMember)}!"
                      )
                      .queue()
                  case fail: Failure[Boolean] ⇒
                    originalMessage.getChannel
                      .sendMessage(
                        s"The database returned a failure, please try again later ${Bot
                          .formatMember(originalMessage.getMember)}."
                      )
                      .queue()
                    fail.exception.printStackTrace()
                }
              case None ⇒
                originalMessage.getChannel
                  .sendMessage(
                    s"You may not create an empty reward, ${Bot.formatMember(originalMessage.getMember)}!"
                  )
                  .queue()
            }
          case parser.NoSuccess(message, in) ⇒
            originalMessage.getChannel
              .sendMessage(
                s"${Bot.formatMember(originalMessage.getMember)} ```${parser
                  .generateErrorOutput(definition, in.pos, message)}```"
              )
              .queue()
        }
      case "delete" | "remove" | "destroy" | "drop" ⇒
        val parser = new RewardParser(originalMessage.getGuild)
        parser.parse(parser.stringConst, definition) match {
          case parser.Success(name: String, _) ⇒
            val identifier =
              RewardIdentifier(originalMessage.getGuild.getIdLong, name)
            new RemoveReward(identifier).mongoQuery.head().onComplete {
              reward: Try[Reward] ⇒
                reward match {
                  case Success(reward: Reward) =>
                    originalMessage.getChannel
                      .sendMessage(
                        s"${Bot.formatMember(originalMessage.getMember)} deleted the reward $name!"
                      )
                      .queue()
                    Main.bot ! RemoveRewardFromMembers(
                      originalMessage.getGuild,
                      reward
                    )
                  case Success(null) ⇒
                    originalMessage.getChannel
                      .sendMessage(
                        s"${Bot.formatMember(originalMessage.getMember)} There is no reward with the name $name!"
                      )
                      .queue()
                  case Failure(throwable) =>
                    originalMessage.getChannel
                      .sendMessage(
                        s"${Bot.formatMember(originalMessage.getMember)} Deletion of the reward $reward failed. please try again later."
                      )
                      .queue()
                    throwable.printStackTrace()
                }
            }
          case parser.NoSuccess(message, in) =>
            originalMessage.getChannel
              .sendMessage(
                s"${Bot.formatMember(originalMessage.getMember)} ```${parser
                  .generateErrorOutput(definition, in.pos, message)}```"
              )
              .queue()
        }
      case "modify" | "edit" | "change" | "update" ⇒
        val parser = new RewardParser(originalMessage.getGuild)
        parser.parse(parser.oneRewardOptionalData(), definition + parser.END) match {
          case parser.Success(newReward: Reward, _) ⇒
            val errors = newReward.isValid(originalMessage.getGuild)
            if (errors.nonEmpty) {
              originalMessage.getChannel
                .sendMessage(
                  s"${Bot.formatMember(originalMessage.getMember)} Validation Errors:\n" +
                    errors.mkString("\n")
                )
                .queue()
              return
            }

            new FindReward(newReward._id).mongoQuery.head().onComplete {
              case Success(oldReward: Reward) =>
                val newRequireTypes = newReward.data.requires.map(_.getClass)
                val newGrantTypes = newReward.data.grants.map(_.getClass)

                val reward: Reward = new Reward(
                  newReward._id,
                  RewardData(
                    newReward.data.requires ++: oldReward.data.requires
                      .filterNot(
                        require ⇒ newRequireTypes.contains(require.getClass)
                      ),
                    newReward.data.grants ++: oldReward.data.grants
                      .filterNot(grant ⇒ newGrantTypes.contains(grant.getClass))
                  ),
                  oldReward.options.changeTo(newReward.options)
                )

                reward.squash match {
                  case Some(changedReward) ⇒
                    if (changedReward.data.grants.isEmpty || changedReward.data.requires.isEmpty) {
                      originalMessage.getChannel
                        .sendMessage(s"You may not create an empty reward, ${Bot
                          .formatMember(originalMessage.getMember)}!")
                        .queue()
                    } else {
                      new ChangeReward(changedReward).mongoQuery
                        .head()
                        .onComplete({
                          case Success(_) ⇒
                            originalMessage.getChannel
                              .sendMessage(
                                s"${Bot.formatMember(originalMessage.getMember)} changed a reward to the definition of `$changedReward`!"
                              )
                              .queue()
                          case Failure(e) => e.printStackTrace()
                        })
                    }
                  case None ⇒
                    originalMessage.getChannel
                      .sendMessage(
                        s"You may not create an empty reward, ${Bot.formatMember(originalMessage.getMember)}!"
                      )
                      .queue()
                    Main.bot ! Recalculate(
                      originalMessage.getGuild,
                      RewardModified()
                    )
                }

              case Success(null) =>
                originalMessage.getChannel
                  .sendMessage(
                    s"There is no reward with that name, ${Bot.formatMember(originalMessage.getMember)}!"
                  )
                  .queue()
              case fail: Failure[_] ⇒
                originalMessage.getChannel
                  .sendMessage(
                    s"The database returned a failure, please try again later ${Bot
                      .formatMember(originalMessage.getMember)}."
                  )
                  .queue()
                fail.exception.printStackTrace()
            }
          case parser.NoSuccess(message, in) ⇒
            originalMessage.getChannel
              .sendMessage(
                s"${Bot.formatMember(originalMessage.getMember)} ```${parser
                  .generateErrorOutput(definition, in.pos, message)}```"
              )
              .queue()
        }
      case option ⇒
        originalMessage.getChannel
          .sendMessage(
            s"${Bot.formatMember(originalMessage.getMember)} Unknown operation $option. Usage: ;reward <create|modify|delete>"
          )
          .queue()
    }
  }
}
