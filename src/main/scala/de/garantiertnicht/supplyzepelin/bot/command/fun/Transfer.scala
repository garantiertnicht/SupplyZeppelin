/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.command.fun

import akka.actor.ActorRef
import akka.util.Timeout
import de.garantiertnicht.supplyzepelin.Main
import de.garantiertnicht.supplyzepelin.bot.{Bot, Transaction}
import de.garantiertnicht.supplyzepelin.bot.command.Command
import de.garantiertnicht.supplyzepelin.persistence.definitions.{
  GetInfoForMember,
  GetScoreboard
}
import de.garantiertnicht.supplyzepelin.persistence.entities.ScoreboardEntry
import de.garantiertnicht.supplyzepelin.persistence.entities.rewards.PointTransfer
import net.dv8tion.jda.api.entities.Message

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Failure, Success}

object Transfer extends Command {
  override val name = "transfer"

  override def execute(
    commandAndArgs: Array[String],
    originalMessage: Message,
    guild: ActorRef
  )(implicit ex: ExecutionContext): Unit = {
    if (commandAndArgs.length < 3) {
      originalMessage.getChannel
        .sendMessage(
          s"You must specify an amount to transfer and at least one user, ${Bot
            .formatMember(originalMessage.getMember)}!."
        )
        .queue()
    } else {
      try {
        val amount = commandAndArgs
          .slice(1, commandAndArgs.length)
          .find(!_.startsWith("<"))
          .getOrElse("-")
          .toLong
        if (amount < 20) {
          originalMessage.getChannel
            .sendMessage(
              s"You must at least transfer 20 points, ${Bot.formatMember(originalMessage.getMember)}!"
            )
            .queue()
          return
        } else if (amount > Main.MAX_POINT_MODIFICATION) {
          originalMessage.getChannel
            .sendMessage(
              Main
                .maxPointsModificationExceededMessage(originalMessage.getMember)
            )
            .queue()
          return
        }

        val users =
          if (originalMessage.mentionsEveryone() || originalMessage.getContentRaw
              .contains("@everyone") || originalMessage.getContentRaw
              .contains("@here")) {
            originalMessage.getGuild.getMembers.asScala
          } else if (originalMessage.getMentionedUsers.size() > 1) {
            originalMessage.getMentionedUsers.asScala.tail
              .map(originalMessage.getGuild.getMember(_))
              .filter(_ != null)
          } else {
            Nil
          }

        if (users.isEmpty) {
          originalMessage.getChannel
            .sendMessage(
              s"📎 It looks like ${Bot.formatMember(originalMessage.getMember)} has sent me creative mentions. Would (s)he/it/space alien like help?"
            )
            .queue()
          return
        }

        val totalAmount = amount * users.length
        implicit val timeout = Timeout(1 minute)

        new GetInfoForMember(originalMessage.getMember).mongoQuery
          .head()
          .onComplete {
            case Success(null) ⇒
              originalMessage.getChannel
                .sendMessage(
                  s"📎 It looks like ${Bot.formatMember(originalMessage.getMember)} tries to create points out of thin air. Would (s)he/it/space alien like help?"
                )
                .queue()
            case Success(entry) =>
              val score = entry.score.total
              if (score >= totalAmount) {
                val tax = 10 + amount / 6
                val amountPerUser = amount - tax
                val totalTax = tax * users.length

                new GetScoreboard(originalMessage.getGuild).mongoQuery
                  .toFuture()
                  .onComplete {
                    case Success(scoreboardEntries) ⇒
                      val entriesFromScoreboard = scoreboardEntries
                        .filter(
                          entry ⇒ users.contains(entry._id.getAsMember().orNull)
                        )

                      val missingOnScoreboard = users
                        .filter(
                          user ⇒
                            !entriesFromScoreboard
                              .exists(_._id.userId == user.getUser.getIdLong)
                        )
                        .map(user ⇒ ScoreboardEntry.default(user))

                      val entries = entriesFromScoreboard ++ missingOnScoreboard

                      val positiveUsers = entries
                        .filter(
                          entry =>
                            entry.score.total >= 0
                              && !entry.blocked.contains(true)
                              && !Bot.isBlocked(entry._id.get.getUser)
                        )
                        .map(_._id.getAsMember())
                        .filter(_.isDefined)
                        .map(_.get)
                      val negativeCount = entries.size - positiveUsers.size
                      val voidAmount = negativeCount * amountPerUser

                      guild ! Transaction(
                        -totalAmount,
                        Seq(originalMessage.getMember),
                        PointTransfer()
                      )
                      guild ! Transaction(
                        amountPerUser,
                        positiveUsers,
                        PointTransfer()
                      )

                      val notifyMentions = if (positiveUsers.length > 10) {
                        positiveUsers
                          .filter(_.getUser.isBot)
                          .map(user => Bot.formatMember(user, true))
                          .mkString(", ") + " and all other users above"
                      } else if (positiveUsers.isEmpty) {
                        "The space aliens"
                      } else {
                        positiveUsers
                          .map(user => Bot.formatMember(user, true))
                          .mkString(", ")
                      }

                      val voidMessage = if (voidAmount == 0) {
                        ""
                      } else {
                        s"-$voidAmount Points (Tribute to space aliens)\n"
                      }

                      originalMessage.getChannel
                        .sendMessage(
                          s"""$notifyMentions has/have each received $amountPerUser points from ${Bot
                               .formatMember(originalMessage.getMember)}! Here is your receipt: ```diff\n
                             |+$totalAmount Points
                             |-$totalTax Points (Transaction Fee)\n$voidMessage
                             |```
                     """.stripMargin
                        )
                        .queue()
                    case _ ⇒
                  }
              } else {
                originalMessage.getChannel
                  .sendMessage(
                    s"You have insufficient funds, ${Bot.formatMember(originalMessage.getMember)}!"
                  )
                  .queue()
                return
              }
            case Failure(exception) =>
              originalMessage.getChannel
                .sendMessage("Getting your score failed. Sorry for that")
                .queue()
              exception.printStackTrace()

          }
      } catch {
        case _: NumberFormatException ⇒
          originalMessage.getChannel
            .sendMessage(
              s"The amount must be numeric, ${Bot.formatMember(originalMessage.getMember)}!"
            )
            .queue()
      }
    }
  }
}
