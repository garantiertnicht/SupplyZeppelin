/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.command.points

import akka.actor.ActorRef
import akka.util.Timeout
import de.garantiertnicht.supplyzepelin.bot.Bot
import de.garantiertnicht.supplyzepelin.bot.command.Command
import de.garantiertnicht.supplyzepelin.bot.utils.Ranker
import de.garantiertnicht.supplyzepelin.persistence.definitions.GetScoreboard
import de.garantiertnicht.supplyzepelin.persistence.entities.ScoreboardEntry
import net.dv8tion.jda.api.entities.{Member, Message}
import net.dv8tion.jda.api.{EmbedBuilder, MessageBuilder}
import org.mongodb.scala.Observer

import scala.collection.mutable
import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

object Scoreboard extends Command {
  override val name: String = "scoreboard"
  implicit val timeout = Timeout(1 minute)

  val blacklistedCharacters = Seq('`', '_', '*', '@')

  override def execute(
    commandAndArgs: Array[String],
    originalMessage: Message,
    guild: ActorRef
  )(implicit ex: ExecutionContext): Unit = {
    val page = if (commandAndArgs.size > 1) {
      val pageString = commandAndArgs(1)

      try {
        math.max(pageString.toInt - 1, 0)
      } catch {
        case numberFormat: NumberFormatException => 0
      }
    } else {
      0
    }

    new GetScoreboard(originalMessage.getGuild).mongoQuery
      .subscribe(new Observer[ScoreboardEntry] {
        var list = mutable.Buffer[ScoreboardEntry]()

        override def onNext(result: ScoreboardEntry) =
          list.prepend(result)

        override def onError(e: Throwable) =
          e.printStackTrace()

        override def onComplete() = {
          var place = 0L
          var lastScore = Long.MaxValue

          val scoreboardString = Ranker
            .rank(
              list.filter(
                entry =>
                  entry.blocked.isEmpty &&
                    !entry._id
                      .getAsMember()
                      .exists(member => Bot.isBlocked(member.getUser))
              ),
              page,
              15
            )
            .map(entry ⇒ {
              s"${entry.place}. ${Bot.formatMember(
                entry.entry._id
                  .getAsMember()
                  .orNull
              )} with ${entry.score} points\n"
            })
            .mkString

          val embed = new EmbedBuilder()
            .setTitle(
              s"Scoreboard for ${originalMessage.getGuild.getName} on page ${page + 1}"
            )
            .setDescription(scoreboardString)
            .setFooter(
              "Points may decay slowly with every negative crate ;-)",
              null
            )
            .build()

          val message = new MessageBuilder()
            .setEmbed(embed)
            .append(Bot.formatMember(originalMessage.getMember))
            .build()

          originalMessage.getChannel.sendMessage(message).queue()
        }
      })
  }
}
