package de.garantiertnicht.supplyzepelin.persistence.definitions

import de.garantiertnicht.supplyzepelin.persistence.entities.{Honorific, UserId}
import de.garantiertnicht.supplyzepelin.persistence.Connection
import net.dv8tion.jda.api.entities.User
import org.mongodb.scala.{
  Completed,
  FindObservable,
  Observable,
  SingleObservable
}
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.UpdateOptions
import org.mongodb.scala.model.Updates._
import org.mongodb.scala.result.DeleteResult

object GetHonorifics extends MongoQuery[Honorific] {
  override val mongoQuery: Observable[Honorific] =
    Connection.honorifics.find()
}

case class SetHonorific(user: UserId, honorific: String)
    extends MongoQuerySingle[Any] {
  override val mongoQuery = {
    if (honorific.isEmpty)
      Connection.honorifics.deleteOne(equal("_id", user))
    else
      Connection.honorifics
        .updateOne(
          equal("_id", user),
          set("honorific", honorific),
          UpdateOptions().upsert(true)
        )
  }
}
