/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.command.management

import akka.actor.ActorRef
import de.garantiertnicht.supplyzepelin.bot.{Bot, Unblock}
import de.garantiertnicht.supplyzepelin.bot.command.Command
import de.garantiertnicht.supplyzepelin.persistence.definitions.UnBlockUser
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.Message

import scala.concurrent.ExecutionContext

object Unblock extends Command {
  override val name: String = "unblock"
  override val permission = Some(Permission.BAN_MEMBERS)

  override def execute(
    commandAndArgs: Array[String],
    originalMessage: Message,
    guild: ActorRef
  )(implicit ex: ExecutionContext): Unit = {
    val users = originalMessage.getMentionedUsers
    if (users.size() > 1) {
      val member = originalMessage.getGuild.getMember(users.get(1))
      if (member != null) {
        new UnBlockUser(member).mongoQuery.head()
        guild ! new Unblock(member.getUser.getIdLong)
        originalMessage.getChannel
          .sendMessage(
            s"${Bot.formatMember(member)} was unblocked by ${Bot.formatMember(originalMessage.getMember)}."
          )
          .queue()
      } else {
        originalMessage.getChannel
          .sendMessage(
            s"The mentioned user is not a member here, ${Bot.formatMember(originalMessage.getMember)}."
          )
          .queue()
      }
    } else {
      originalMessage.getChannel
        .sendMessage(
          s"You must mention a member to unblock, ${Bot.formatMember(originalMessage.getMember)}!"
        )
        .queue()
    }
  }
}
