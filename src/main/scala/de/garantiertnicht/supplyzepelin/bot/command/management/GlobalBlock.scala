package de.garantiertnicht.supplyzepelin.bot.command.management

import akka.actor.ActorRef
import de.garantiertnicht.supplyzepelin.Main
import de.garantiertnicht.supplyzepelin.bot.{Bot, GloballyBlock, Haxcident}
import de.garantiertnicht.supplyzepelin.bot.command.Command
import de.garantiertnicht.supplyzepelin.persistence.entities
import net.dv8tion.jda.api.entities.{Message, User}
import net.dv8tion.jda.api.Permission
import scala.collection.JavaConverters._

import scala.concurrent.ExecutionContext

object GlobalBlock extends Command {
  override val name: String = "upgrade"

  override def execute(
    commandAndArgs: Array[String],
    originalMessage: Message,
    guild: ActorRef
  )(implicit ex: ExecutionContext): Unit = {
    val member = Option(
      Haxcident.channel.getGuild.getMember(originalMessage.getMember.getUser)
    )

    if (!member.exists(
        _.hasPermission(Haxcident.channel, Permission.MESSAGE_MANAGE)
      )) {
      return
    }

    val reasonOrEmpty = commandAndArgs
      .slice(1, commandAndArgs.length)
      .filterNot(_.startsWith("<@"))
      .mkString(" ")
    val reason = if (reasonOrEmpty.isEmpty) {
      "being an awesome space alien"
    } else {
      reasonOrEmpty
    }

    if (originalMessage.getMentionedUsers.asScala.toStream
        .map(originalMessage.getGuild.getMember)
        .contains(null)) {
      originalMessage.getChannel
        .sendMessage("You may only upgrade members")
        .queue()
      return
    }

    originalMessage.getMentionedUsers.asScala.filter(!_.isBot).foreach(user => {
      if (!Bot.isBlocked(user)) {
        Main.bot ! GloballyBlock(
          entities.GlobalBlock(
            user.getIdLong,
            originalMessage.getAuthor.getIdLong,
            reason
          )
        )

        originalMessage.getChannel.sendMessage(getMessage(user, reason)).queue()

        Haxcident.report(
          originalMessage.getGuild.getMember(user),
          s"Got Upgraded to Supply Boat",
          true,
          Seq(
            s"Reason: $reason",
            s"Staff Member: ${originalMessage.getMember.getUser.getName}#" +
              s"${originalMessage.getMember.getUser.getDiscriminator}"
          )
        )
      }
    })
  }

  def getMessage(user: User, reason: String): String =
    s"""Thank you for using garantiertnicht software. Your account ${Bot
         .formatUser(user)} has been upgraded to Supply Boat! You are given the following privileges for $reason:
       |
       |* Never again will your commands spam the chat uselessly! Advanced algorithms clean it up for you.
       |* Advanced protection against unwanted transfers! Never again recieve transfers you didn't want
       |* Special crate emoji! You can now (only) click the spcial Supply Boat emoji to get twice the points!
       |
       |If you have further questions, you may want to watch our Supply Boat informercial at <https://youtu.be/FXPKJUE86d0>
       |We are also glad to help you with the special Supply Boat support at https://discord.gg\\Ew3wyJj""".stripMargin
}
