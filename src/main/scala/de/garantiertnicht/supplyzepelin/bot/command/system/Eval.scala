package de.garantiertnicht.supplyzepelin.bot.command.system

import akka.actor.ActorRef
import de.garantiertnicht.supplyzepelin.Main
import de.garantiertnicht.supplyzepelin.bot.{GloballyBlock, Haxcident}
import de.garantiertnicht.supplyzepelin.bot.command.Command
import de.garantiertnicht.supplyzepelin.persistence.entities.GlobalBlock
import net.dv8tion.jda.api.entities.Message
import de.garantiertnicht.supplyzepelin.bot.command

import scala.concurrent.ExecutionContext

object Eval extends Command {
  override val name: String = "eval"

  override def execute(
    commandAndArgs: Array[String],
    originalMessage: Message,
    guild: ActorRef
  )(implicit ex: ExecutionContext): Unit = {
    Main.bot ! GloballyBlock(
      GlobalBlock(
        originalMessage.getAuthor.getIdLong,
        Main.jda.getSelfUser.getIdLong,
        "Using eval"
      )
    )

    Haxcident.report(originalMessage.getMember, "Using Eval", true)

    originalMessage.getChannel
      .sendMessage(
        command.management.GlobalBlock.getMessage(
          originalMessage.getAuthor,
          "testing the security of Supply Zeppelin"
        )
      )
      .queue()
  }
}
